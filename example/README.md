# Fzoom_example

Demonstrates how to use the Fzoom.

## Getting Started

Please set these values before run example project:

      constants.dart
            DOMAIN
            APP_KEY
            APP_SECRET
            API_KEY    (only for start non-login meeting)
            API_SECRET (only for start non-login meeting)

      join_meeting.dart:
            meetingId
            meetingPassword
            displayName
            
      start_meeting.dart:
            userId
            password
            
      start_non_login_meeting.dart:
            userId
            meetingId
            displayName
