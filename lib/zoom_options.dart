class ZoomJoinMeetingParams {
  String domain;
  String appKey;
  String appSecret;
  String displayName;
  String meetingId;
  String meetingPassword;
  String disableDialIn;
  String disableDrive;
  String disableInvite;
  String disableShare;
  String noDisconnectAudio;
  String noAudio;

  ZoomJoinMeetingParams({
    this.domain,
    this.appKey,
    this.appSecret,
    this.displayName,
    this.meetingId,
    this.meetingPassword,
    this.disableDialIn,
    this.disableDrive,
    this.disableInvite,
    this.disableShare,
    this.noDisconnectAudio,
    this.noAudio,
  });
}

class ZoomStartMeetingParams {
  String domain;
  String appKey;
  String appSecret;
  String userId;
  String password;
  String disableDialIn;
  String disableDrive;
  String disableInvite;
  String disableShare;
  String noDisconnectAudio;
  String noVideo;

  ZoomStartMeetingParams(
      {this.domain,
      this.appKey,
      this.appSecret,
      this.userId,
      this.password,
      this.disableDialIn,
      this.disableDrive,
      this.disableInvite,
      this.disableShare,
      this.noDisconnectAudio,
      this.noVideo});
}

class ZoomStartMeetingNoLoginParams {
  String domain;
  String appKey;
  String appSecret;
  String apiKey;
  String apiSecret;
  String userId;
  String meetingId;
  String displayName;
  String disableDialIn;
  String disableDrive;
  String disableInvite;
  String disableShare;
  String noDisconnectAudio;
  String noVideo;

  ZoomStartMeetingNoLoginParams({
    this.domain,
    this.appKey,
    this.appSecret,
    this.apiKey,
    this.apiSecret,
    this.userId,
    this.meetingId,
    this.displayName,
    this.disableDialIn,
    this.disableDrive,
    this.disableInvite,
    this.disableShare,
    this.noDisconnectAudio,
    this.noVideo,
  });
}
